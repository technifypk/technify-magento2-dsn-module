<?php

namespace Technify\Dsn\Observer\Product;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class UpdateProduct implements ObserverInterface{



    /** @var \Technify\Dsn\Helper\Help $helper*/
    public $helper;

    /** @var \Magento\Framework\ObjectManagerInterface $_objectManager*/
    private $_objectManager;

    /** @var \Magento\Sales\Model\OrderRepository $_orderRepository*/
    private $_orderRepository;

    /** @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $_configurable*/
    private $_configurable;


    /**
     * UpdateProduct constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Technify\Dsn\Helper\Help $helper
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Technify\Dsn\Helper\Help $helper,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
    )
    {
        $this->_configurable = $catalogProductTypeConfigurable;
        $this->_orderRepository = $orderRepository;
        $this->_objectManager = $objectManager;
        $this->helper = $helper;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getData("product");

        $entityId = [$product->getId()];
        $isProductHasParent = $this->_configurable->getParentIdsByChild($entityId);

        if (!empty($isProductHasParent) && $isProductHasParent)
        {
            $entityId = $isProductHasParent;
        }

        $this->pushProduct("product",$entityId,"update",$this->helper->getTechnifyStoreId(),"pushUpdate");
    }

    /**
     * @param string $entity
     * @param string|int|int[]|string[] $entityId
     * @param string $event
     * @param string $storeId
     * @param string $action
     */
    public function pushProduct($entity, $entityId, $event, $storeId, $action)
    {
        $data['entity'] = array(
            'name'  	=> $entity,
            'id'    	=> $entityId,
            'event' 	=> $event,
            'store_id'	=> $storeId
        );
        $this->_objectManager->get('\Technify\Dsn\Helper\RequestGateway')->push($action,$data);

    }
}