<?php

namespace Technify\Dsn\Model;

use Technify\Dsn\Api\OrderInterface;

/**
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
*/
class Order extends \Technify\Dsn\Helper\Order implements OrderInterface
{


    /** @var \Magento\Checkout\Model\Cart $_cart*/
    private $_cart;

    /** @var \Magento\Customer\Helper\Session\CurrentCustomer $_currentCustomer*/
    private $_currentCustomer;

    /** @var \Magento\Framework\ObjectManagerInterface $_objectManager*/
    private $_objectManager;

    /** @var \Magento\Quote\Model\QuoteManagement $_quoteManagement*/
    private $_quoteManagement;

    /** @var \Magento\Sales\Model\OrderRepository $_orderRepository */
    private $_orderRepository;

    /** @var \Magento\Store\Model\Store $_store */
    private $_store;

    /** @var \Magento\Framework\DataObject\Factory $_objectFactory */
    public $_objectFactory;

    /** @var \Technify\Dsn\Helper\Help $_helper*/
    private $_helper;

    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Customer\Helper\Session\CurrentCustomer $customer,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Store\Model\Store $store,
        \Magento\Framework\DataObject\Factory $objectFactory,
        \Technify\Dsn\Helper\Help $helper
    )
    {
        $this->_helper = $helper;
        $this->_objectFactory = $objectFactory;
        $this->_store = $store;
        $this->_orderRepository = $orderRepository;
        $this->_quoteManagement = $quoteManagement;
        $this->_objectManager = $objectManager;
        $this->_cart = $cart;
        $this->_currentCustomer = $customer;
        parent::__construct($quoteManagement,$orderRepository,$objectFactory,$objectManager);
    }


    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param \Magento\Quote\Api\Data\AddressInterface $shippingAddress
     * @param \Magento\Quote\Api\Data\AddressInterface $billingAddress
     * @param string $shippingMethod
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\CartItemInterface[] $items
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function create(
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        \Magento\Quote\Api\Data\AddressInterface $shippingAddress,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress,
        $shippingMethod,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        array $items
    )
    {
        $order = $this->_initQuote($this->_cart)
            ->setStore($this->_store->load(1))
            ->setCustomer($this->_currentCustomer->getCustomerId(),$customer)
            ->addProductsToCart($items)
            ->setBillingAddress($billingAddress)
            ->setShippingAddress($shippingAddress)
            ->setShippingMethod($shippingMethod)
            ->setPaymentMethod($paymentMethod)
            ->collectTotals()
            ->placeOrder();

        $this->_helper->setTechnifyOrder($order->getIncrementId());

        return $order;
    }


    /**
     * @param string $orderId
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param \Magento\Quote\Api\Data\AddressInterface $shippingAddress
     * @param \Magento\Quote\Api\Data\AddressInterface $billingAddress
     * @param string $shippingMethod
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\CartItemInterface[] $items
     * @param \Technify\Dsn\Api\OrderStatusInterface $orderStatus
     * @return \Magento\Sales\Api\Data\OrderInterface
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function update(
        $orderId,
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        \Magento\Quote\Api\Data\AddressInterface $shippingAddress,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress,
        $shippingMethod,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        array $items,
        \Technify\Dsn\Api\OrderStatusInterface $orderStatus
    )
    {
        $order = $this->_objectManager
            ->create(
                'Magento\Sales\Model\Order'
            )->loadByIncrementId($orderId);

        $quoteId = $order->getQuoteId();

        $this
            ->setOrder($order)
            ->setQuote($quoteId)
            ->setCart($this->_cart)
            ->updateOrderCustomer($customer)
            ->updateOrderShippingAddress($shippingAddress)
            ->updateOrderBillingAddress($billingAddress)
            ->updateOrderAssignedQuote(
                $customer,$items,$shippingAddress,$billingAddress,$shippingMethod,$paymentMethod
            )->removeOrderItems()
            ->addProductsOrder()
            ->updateTotals()
            ->saveOrder();

        $this->updateOrderStatus($orderStatus);

        return $this->getOrder();
    }

    /**
     * @param string $orderId
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function get($orderId)
    {
        $incrementId = $this->_helper->getOrderLastChild($orderId);

        $order = $this->_objectManager
            ->create(
                'Magento\Sales\Model\Order'
            )->loadByIncrementId($incrementId);

        $orderResponse = $this->_orderRepository->get(
            $order->getId()
        );

        $orderResponse->setIncrementId($orderId);

        return $orderResponse;
    }
}