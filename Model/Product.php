<?php


namespace Technify\Dsn\Model;

use Technify\Dsn\Api\ProductInterface;

class Product implements ProductInterface{

    /** @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory */
    private $productCollectionFactory;

    /** @var \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus */
    private $productStatus;

    /** @var \Magento\Catalog\Model\Product\Visibility $productVisibility */
    private $productVisibility;

    /** @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $_configurable*/
    private $_configurable;

    /**@var \Magento\Framework\ObjectManagerInterface $_objectManager*/
    private $_objectManager;

    /** @var \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository*/
    private $productAttributeRepository;

    /** @var \Magento\Eav\Model\AttributeManagement $attributeManagement*/
    private $attributeManagement;

    /**@var \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor*/
    private $extensionAttributesJoinProcessor;

    /**
     * Product constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus
     * @param \Magento\Catalog\Model\Product\Visibility $productVisibility
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository,
        \Magento\Eav\Model\AttributeManagement $attributeManagement,
        \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor
    ) {
        $this->_objectManager = $objectManager;
        $this->_configurable = $catalogProductTypeConfigurable;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->attributeManagement = $attributeManagement;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
    }

    /**
     * @return array
     */
    public function get()
    {
        $productTypeInstance = $this->_objectManager->get('Magento\ConfigurableProduct\Model\Product\Type\Configurable');

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->productCollectionFactory->create();
        $collection->_loadAttributes()->addAttributeToSelect("*")
            ->addOptionsToResult()
            ->addCategoryIds()
            ->addMediaGalleryData()
            ->addFilterByRequiredOptions()
            ->addStoreFilter(1)->load();

        $productsCollection = $collection->getItems();

        $products = array();

        foreach ($productsCollection as $product)
        {
            /** @var \Magento\Catalog\Model\Product $product*/
            if(!$this->_configurable->getParentIdsByChild($product->getId()))
            {
                $variants = array();

                if ($product->getTypeId() == "configurable")
                {
                    $usedProducts = $productTypeInstance
                        ->getUsedProductCollection($product)
                        ->addAttributeToSelect("*")
                        ->addMediaGalleryData()
                        ->addStoreFilter(1)
                        ->getItems();

                    foreach ($usedProducts as $variant)
                    {
                        /** @var \Magento\Catalog\Model\Product $variant*/
                        $variants[] = array(
                            'product_id'    => $product->getId(),
                            'variant_id'    => $variant->getId(),
                            'sku'           => $variant->getSku(),
                            'url'           => $variant->getProductUrl(),
                            'attribute_set_id' => $variant->getAttributeSetId(),
                            'price'         => $variant->getPrice(),
                            'status'        => $variant->getStatus(),
                            'visibility'    => $variant->getVisibility(),
                            'type_id'       => $variant->getTypeId(),
                            'created_at'    => $variant->getCreatedAt(),
                            'updated_at'    => $variant->getUpdatedAt(),
                            'weight'        => $variant->getWeight(),
                            'options'       => $variant->getProductOptionsCollection()->getData(),
                            'qty'           => $variant->getQty(),
                            'available'     => $variant->isAvailable(),
                            'in_stock'      => $variant->isInStock(),
                            'is_virtual'    => $variant->isVirtual(),
                            'name'         => $variant->getName(),
                            'final_price'   => $variant->getFinalPrice(),
                            'minimal_price' => $variant->getMinimalPrice(),
                            'special_price' => array(
                                'special_price_from_date'   => $variant->getSpecialFromDate(),
                                'special_price_to_date'     => $variant->getSpecialToDate(),
                                'special_price' => $variant->getSpecialPrice()
                            ),
                            'media_gallery_images'      => $variant->getMediaGalleryImages()->toArray(),
                            'calculated_final_price'    => $variant->getCalculatedFinalPrice(),
                            'upsell_product_ids'        => $variant->getUpSellProductIds(),
                            'tier_price'                => $variant->getTierPrice(),
                            'attributes'                => $this->getAttributes(
                                $productTypeInstance->getUsedProductAttributeIds($product),
                                $productTypeInstance->getConfigurableAttributesAsArray($product),
                                $variant
                            )
                        );
                    }
                }
                $products[] = array(
                    'product_id'        => $product->getId(),
                    'sku'               => $product->getSku(),
                    'description'       => $product->getData('description'),
                    'short_description' => $product->getData('short_description'),
                    'meta_title'        => $product->getData('meta_title'),
                    'meta_keywords'     => $product->getData('meta_keywords'),
                    'meta_description'  => $product->getData('meta_description'),
                    'url'               => $product->getProductUrl(),
                    'attribute_set_id'  => $product->getAttributeSetId(),
                    'price'         => $product->getPrice(),
                    'status'        => $product->getStatus(),
                    'visibility'    => $product->getVisibility(),
                    'type_id'       => $product->getTypeId(),
                    'created_at'    => $product->getCreatedAt(),
                    'updated_at'    => $product->getUpdatedAt(),
                    'weight'        => $product->getWeight(),
                    'options'       => $product->getProductOptionsCollection()->toArray(),
                    'variants'      => $variants,
                    'website_ids'   => $product->getWebsiteIds(),
                    'store'         => $product->getStore()->toArray(),
                    'qty'           => $product->getQty(),
                    'available'     => $product->isAvailable(),
                    'in_stock'      => $product->isInStock(),
                    'is_virtual'    => $product->isVirtual(),
                    'title'         => $product->getName(),
                    'final_price'   => $product->getFinalPrice(),
                    'minimal_price' => $product->getMinimalPrice(),
                    'special_price' => array(
                        'special_price_from_date'   => $product->getSpecialFromDate(),
                        'special_price_to_date'     => $product->getSpecialToDate(),
                        'special_price' => $product->getSpecialPrice()
                    ),
                    'media_gallery_images'      => $product->getMediaGalleryImages()->toArray(),
                    'calculated_final_price'    => $product->getCalculatedFinalPrice(),
                    'upsell_product_ids'        => $product->getUpSellProductIds(),
                    'tier_price'                => $product->getTierPrice()
                );
            }
        }
        return $products;
    }


    /**
     * @param array $getUsedProductAttributeIds
     * @param array $getConfigurableAttributesAsArray
     * @param \Magento\Catalog\Model\Product $variant
     * @return array
     */
    public function getAttributes(
        array $getUsedProductAttributeIds,
        array $getConfigurableAttributesAsArray,
        \Magento\Catalog\Model\Product $variant
    )
    {
        $attributes = array();
        foreach ($getUsedProductAttributeIds as $getUsedProductAttributeId)
        {
            $id     = $getConfigurableAttributesAsArray[$getUsedProductAttributeId]['attribute_id'];
            $label  = $getConfigurableAttributesAsArray[$getUsedProductAttributeId]['label'];

            $attributeValue = $this->getAttributeValue(
                $getConfigurableAttributesAsArray[$getUsedProductAttributeId]['values'],
                $variant->getData($getConfigurableAttributesAsArray[$getUsedProductAttributeId]['attribute_code'])
            );

            $attributes[] = array(
                'attribute_id'          => $id,
                'attribute_label'       => $label,
                'attribute_value_id'    => $attributeValue['value_index'],
                'attribute_value_label' => $attributeValue['label']
            );

        }

        return $attributes;
    }

    /**
     * @param $values
     * @param $valueIndex
     * @return mixed
     */
    public function getAttributeValue($values, $valueIndex)
    {
        $attributeValue = array();

        foreach ($values as $value)
        {
            if ($value['value_index']==$valueIndex)
            {
                $attributeValue = $value;
                break;
            }
        }
        return $attributeValue;
    }
}