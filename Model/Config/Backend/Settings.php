<?php

namespace Technify\Dsn\Model\Config\Backend;

use Magento\Framework\App\Config\Value;


class Settings extends Value{

    /** @var \Magento\Framework\ObjectManagerInterface $_objectManager*/
    private $_objectManager;

    /** @var \Magento\Framework\Message\ManagerInterface $_messageManager*/
    private $_messageManager;

    /** @var \Magento\Framework\App\Request\Http $request*/
    public $request;

    /**
     * Settings constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource= null,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        \Magento\Framework\App\Request\Http $request,
        array $data = [])
    {
        $this->request = $request;
        $this->_messageManager = $messageManager;
        $this->_objectManager = $objectManager;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    public function beforeSave()
    {
        $accessToken = $this->getData('groups/technify_section_settings/fields/technify_section_settings_api_key/value');

        $params = array(
            'store_id' => $this->getData('groups/technify_section_settings/fields/technify_section_settings_store_id/value'),
            'token'    => $accessToken
        );

        $response = json_decode($this->_objectManager->get('\Technify\Dsn\Helper\RequestGateway')->authorize($params,$accessToken),true);

        if (isset($response['authorize']) && !$response['authorize'])
        {
            $this->_dataSaveAllowed = false;
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Authorization Failed')
            );
        }

        return parent::beforeSave();
    }

}