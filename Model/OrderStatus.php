<?php

namespace Technify\Dsn\Model;

class OrderStatus implements \Technify\Dsn\Api\OrderStatusInterface {

    public $status;
    public $comment;
    public $notify;
    public $trackingCarrier;
    public $trackingId;
    public $state;

    /** @var \Magento\Sales\Api\Data\ShipmentTrackInterface[] $tracks */
    public $tracks;

    /**
     * @param boolean $status
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param boolean $notify
     * @return $this
     */
    public function setNotify($notify)
    {
        $this->notify = $notify;
    }

    /**
     * @return boolean
     */
    public function getNotify()
    {
        return $this->notify;
    }

    /**
     * @param string $status
     * @return void
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param \Magento\Sales\Api\Data\ShipmentTrackInterface[] $tracks
     * @return mixed
     */
    public function setTracks(array $tracks)
    {
        $this->tracks = $tracks;
    }

    /**
     * @return \Magento\Sales\Api\Data\ShipmentTrackInterface[] $tracks
     */
    public function getTracks()
    {
        return $this->tracks;
    }
}