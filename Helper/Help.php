<?php

namespace Technify\Dsn\Helper;

class Help{

    /** @var \Magento\Framework\App\ResourceConnection $_resourceConnection*/
    private $_resourceConnection;

    /** @var \Magento\Framework\App\ResourceConnection $_scopeConfig*/
    private $_scopeConfig;

    const TECHNIFY_ID_PATH = "technify_section/technify_section_settings/technify_section_settings_store_id";
    const TECHNIFY_AUTH_TOKEN = "technify_section/technify_section_settings/technify_section_settings_api_key";

    /**
     * Help constructor.
     * @param \Magento\Framework\App\ObjectManager $objectManager
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_resourceConnection = $resourceConnection;
    }

    /**
     * @param string $incrementId
     * @return string
     */
    public function getOrderLastChild($incrementId)
    {
        $connection = $this->_resourceConnection->getConnection();

        $collection = $connection->select()
            ->from(
                array("msof" => $this->_resourceConnection->getTableName('sales_order')),
                array("increment_id")
            )->where("original_increment_id = ?",$incrementId)
            ->where("created_at = ?",$connection->select()
                ->from(
                    $this->_resourceConnection->getTableName('sales_order'),
                    array("MAX(created_at)"))
                ->where("original_increment_id = ?","msof.original_increment_id")
            )->limit(1);

        $childIncrementId = $connection->fetchOne($collection);
        return (($childIncrementId) ? $childIncrementId : $incrementId);
    }

    /**
     * @param string $incrementId
     * @return string
     */
    public function getParentOrderId($incrementId)
    {
        $connection = $this->_resourceConnection->getConnection();

        $collection = $connection->select()
            ->from(
                $this->_resourceConnection->getTableName('sales_order'),
                array("original_increment_id")
            )->where("increment_id = ?",$incrementId)->limit(1);

        $parentIncrementId = $connection->fetchOne($collection);
        return ($parentIncrementId) ? $parentIncrementId : $incrementId;
    }

    /**
     * @return mixed
     */
    public function getTechnifyStoreId()
    {
        $storeScope =  \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        return $this->_scopeConfig->getValue(self::TECHNIFY_ID_PATH, $storeScope);
    }

    /**
     * @return mixed
     */
    public function getTechnifyAuthToken()
    {
        $storeScope =  \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        return $this->_scopeConfig->getValue(self::TECHNIFY_AUTH_TOKEN, $storeScope);
    }

    /**
     * @param string $incrementId
     * @return bool
     */
    public function isTechnifyOrder($incrementId)
    {
        $connection = $this->_resourceConnection->getConnection();

        $collection = $connection->select()
            ->from(
                $this->_resourceConnection->getTableName('technify_dsn_order'),
                array("is_technify_order")
            )->where("order_id = ?",$incrementId)->limit(1);

        $isTechnifyOrder = $connection->fetchOne($collection);
        return ($isTechnifyOrder) ? true : false;
    }

    /**
     * @param string $incrementId
     */
    public function setTechnifyOrder($incrementId)
    {
        $connection = $this->_resourceConnection->getConnection();

        try{
            $connection->beginTransaction();
            $connection->insert($this->_resourceConnection->getTableName('technify_dsn_order'),array('order_id' => $incrementId, 'is_technify_order' => 1));
            $connection->commit();
        }catch (\Exception $e){
            $connection->rollBack();
        }
    }
}