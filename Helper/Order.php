<?php

namespace Technify\Dsn\Helper;

use \Magento\Sales\Model\Order as SalesOrderModel;

class Order{

    private $_availableShippingMethodCode = null;

    /** @var \Magento\Quote\Model\Quote $this->_quote */
    private $_quote;

    /** @var \Magento\Checkout\Model\Cart $_cart*/
    private $_cart;


    /** @var \Magento\Quote\Model\QuoteManagement $_quoteManagement*/
    private $_quoteManagement;

    /** @var \Magento\Sales\Model\OrderRepository $_orderRepository */
    private $_orderRepository;

    /** @var \Magento\Framework\DataObject\Factory $_objectFactory */
    public $_objectFactory;

    /** @var \Magento\Sales\Model\Order $order */
    private $order;

    /** @var \Magento\Framework\ObjectManagerInterface $_objectManager */
    private $_objectManager;

    /**
     * Order constructor.
     * @param \Magento\Quote\Model\QuoteManagement $quoteManagement
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Framework\DataObject\Factory $objectFactory
     */
    public function __construct(
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Framework\DataObject\Factory $objectFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        $this->_objectManager = $objectManager;
        $this->_objectFactory = $objectFactory;
        $this->_orderRepository = $orderRepository;
        $this->_quoteManagement = $quoteManagement;
    }

    /**
     * @param int $quote
     * @return $this
     */
    public function setQuote(int $quote)
    {
        $this->_quote = $this->_objectManager->get(\Magento\Quote\Model\Quote::class)->load($quote);
        return $this;
    }

    /**
     * Declare quote store model
     *
     * @param \Magento\Store\Model\Store $store
     * @return $this
     */
    public function setStore($store)
    {
        $this->_quote->setStore($store);
        return $this;
    }


    /**
     * @param string $shippingMethodCode
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getShippingMethodList($shippingMethodCode)
    {
        $availableShippingMethods = $this->_quote->getShippingAddress()->getShippingRatesCollection();

        if (sizeof($availableShippingMethods) > 0)
        {
            foreach ($availableShippingMethods as $shippingMethod)
            {
                if ($shippingMethod->getCarrier() . "_" .$shippingMethod->getMethod() == $shippingMethodCode)
                {
                    $this->_availableShippingMethodCode = $shippingMethodCode;
                }
            }
        }else{
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Shipping Methods are not available for this quote.')
            );
        }

        return $availableShippingMethods;
    }

    /**
     * @param string $shippingMethodCode
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function setShippingMethod($shippingMethodCode)
    {
        $this->_quote
            ->getShippingAddress()
            ->setCollectShippingRates(true)
            ->collectShippingRates();

        $availableShippingMethods = $this->getShippingMethodList($shippingMethodCode);

        if ($this->_availableShippingMethodCode==null)
        {
            $this->_availableShippingMethodCode = $availableShippingMethods[0]->getCarrier() . "_" . $availableShippingMethods[0]->getMethod();
        }

        $this->_quote->getShippingAddress()->setShippingMethod($this->_availableShippingMethodCode);
        return $this;
    }

    /**
     * @param \Magento\Checkout\Model\Cart $cart
     * @return $this
     */
    protected function _initQuote(\Magento\Checkout\Model\Cart $cart)
    {
        $this->_cart = $cart;
        $this->_quote = $this->_cart->getQuote();
        $this->_quote->setStoreId(1)
            ->setWebsite(1);
        return $this;
    }

    /**
     * @param \Magento\Checkout\Model\Cart $cart
     * @return $this
     */
    public function setCart($cart)
    {
        $this->_cart = $cart;
        $this->_cart->setQuote($this->_quote);
        return $this;
    }

    /**
     * @param \Magento\Quote\Api\Data\CartItemInterface[] $items
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addProductsToCart(array $items)
    {
        $this->_quote->removeAllItems()->setItemsCount(0);

        foreach ($items as $item)
        {
            $requestInfo = array();
            $productId = $item->getItemId();
            $requestInfo['qty'] = $item->getQty();
            if ($item->getProductOption() != null && sizeof($item->getProductOption()->getExtensionAttributes()->getConfigurableItemOptions()) > 0)
            {
                $options = array();
                foreach ($item->getProductOption()->getExtensionAttributes()->getConfigurableItemOptions() as $configurableItemOption)
                {
                    $options[$configurableItemOption->getOptionId()] = $configurableItemOption->getOptionValue();
                }
                $requestInfo['super_attribute'] = $options;
            }

            $this->_cart->addProduct($productId,$requestInfo);
        }

        return $this;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return $this
     */
    public function updateOrderCustomer(\Magento\Customer\Api\Data\CustomerInterface $customer)
    {
        $this->order
            ->setCustomerEmail($customer->getEmail())
            ->setCustomerFirstname($customer->getFirstname())
            ->setCustomerMiddlename($customer->getMiddlename())
            ->setCustomerLastname($customer->getLastname())
            ->setCustomerIsGuest(true);
        return $this;
    }

    /**
     * @param \Magento\Quote\Api\Data\AddressInterface $shippingAddress
     * @return $this
     */
    public function updateOrderShippingAddress(\Magento\Quote\Api\Data\AddressInterface $shippingAddress)
    {
        $this->order
            ->getShippingAddress()
            ->setTelephone($shippingAddress->getTelephone())
            ->setFirstname($shippingAddress->getFirstname())
            ->setMiddlename($shippingAddress->getMiddlename())
            ->setLastname($shippingAddress->getLastname())
            ->setStreet($shippingAddress->getStreet())
            ->setCity($shippingAddress->getCity())
            ->setCountryId($shippingAddress->getCountryId())
            ->setRegion($shippingAddress->getRegion())
            ->setPostcode($shippingAddress->getPostcode());

        return $this;
    }

    /**
     * @param \Magento\Quote\Api\Data\AddressInterface $billingAddress
     * @return $this
     */
    public function updateOrderBillingAddress(\Magento\Quote\Api\Data\AddressInterface $billingAddress)
    {

        $this->order
            ->getBillingAddress()
            ->setTelephone($billingAddress->getTelephone())
            ->setFirstname($billingAddress->getFirstname())
            ->setMiddlename($billingAddress->getMiddlename())
            ->setLastname($billingAddress->getLastname())
            ->setStreet($billingAddress->getStreet())
            ->setCity($billingAddress->getCity())
            ->setCountryId($billingAddress->getCountryId())
            ->setRegion($billingAddress->getRegion())
            ->setPostcode($billingAddress->getPostcode());

        return $this;
    }


    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param \Magento\Quote\Api\Data\CartItemInterface[] $items
     * @param \Magento\Quote\Api\Data\AddressInterface $shippingAddress
     * @param \Magento\Quote\Api\Data\AddressInterface $billingAddress
     * @param string $shippingMethod
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateOrderAssignedQuote($customer, $items, $shippingAddress, $billingAddress, $shippingMethod, $paymentMethod)
    {
        $this->setQuote($this->order->getQuoteId())
            ->setCustomer($this->order->getCustomerId(),$customer)
            ->addProductsToCart($items)
            ->setShippingAddress($shippingAddress)
            ->setBillingAddress($billingAddress)
            ->setShippingMethod($shippingMethod)
            ->setPaymentMethod($paymentMethod)
            ->collectTotals()
            ->saveQuote();

        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function removeOrderItems()
    {
        foreach ($this->order->getAllItems() as $item)
        {
            $item->delete();
        }
        $this->order->setTotalItemCount(0);
        return $this;
    }

    /**
     * @return $this
     */
    public function addProductsOrder()
    {
        /** @var \Magento\Quote\Model\Quote\Item\ToOrderItem $quoteToOrderItem */
        $quoteToOrderItem = $this->_objectManager->get(\Magento\Quote\Model\Quote\Item\ToOrderItem::class);

        foreach ($this->_quote->getAllItems() as $quoteItem)
        {
            /** @var \Magento\Quote\Model\Quote\Item $quoteItem */
            $orderItem = $quoteToOrderItem->convert($quoteItem);
            $this->order->setItems(array_merge($this->order->getItems(), [$orderItem]));
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function updateTotals()
    {
        $this->order->setBaseCurrencyCode($this->_quote->getBaseCurrencyCode());
        $this->order->setBaseGrandTotal($this->_quote->getBaseGrandTotal());
        $this->order->setBaseShippingAmount($this->_quote->getShippingAddress()->getBaseShippingAmount());
        $this->order->setBaseShippingDiscountAmount($this->_quote->getShippingAddress()->getBaseShippingDiscountAmount());
        $this->order->setBaseShippingInclTax($this->_quote->getShippingAddress()->getBaseShippingInclTax());
        $this->order->setBaseShippingTaxAmount($this->_quote->getShippingAddress()->getBaseShippingTaxAmount());
        $this->order->setBaseSubtotal($this->_quote->getBaseSubtotal());
        $this->order->setBaseToGlobalRate($this->_quote->getBaseToGlobalRate());
        $this->order->setBaseToOrderRate($this->_quote->getBaseToQuoteRate());
        $this->order->setBillingAddressId($this->_quote->getBillingAddress()->getId());
        $this->order->setGrandTotal($this->_quote->getGrandTotal());
        $this->order->setGlobalCurrencyCode($this->_quote->getGlobalCurrencyCode());
        $this->order->setOrderCurrencyCode($this->_quote->getQuoteCurrencyCode());
        $this->order->setShippingAmount($this->_quote->getShippingAddress()->getShippingAmount());
        $this->order->setShippingDescription($this->_quote->getShippingAddress()->getShippingDescription());
        $this->order->setShippingDiscountAmount($this->_quote->getShippingAddress()->getShippingDiscountAmount());
        $this->order->setShippingInclTax($this->_quote->getShippingAddress()->getShippingInclTax());
        $this->order->setShippingTaxAmount($this->_quote->getShippingAddress()->getShippingTaxAmount());
        $this->order->setStoreToBaseRate($this->_quote->getStoreToBaseRate());
        $this->order->setStoreCurrencyCode($this->_quote->getStoreCurrencyCode());
        $this->order->setStoreToOrderRate($this->_quote->getStoreToQuoteRate());
        $this->order->setSubtotal($this->_quote->getSubtotal());
        $this->order->setTotalItemCount($this->_quote->getItemsCount());
        $this->order->setTotalQtyOrdered($this->_quote->getItemsQty());
        $this->order->setWeight($this->_quote->getShippingAddress()->getWeight());
        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function saveOrder()
    {
        $this->order->save();
        return $this;
    }

    /**
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @return $this
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->_quote->setPayment($paymentMethod);
        return $this;
    }

    /**
     * @param \Magento\Quote\Api\Data\AddressInterface $shippingAddress
     * @return $this
     */
    public function setShippingAddress(\Magento\Quote\Api\Data\AddressInterface $shippingAddress)
    {
        $this->_quote->setShippingAddress($shippingAddress);
        return $this;
    }

    /**
     * @param \Magento\Quote\Api\Data\AddressInterface $billingAddress
     * @return $this
     */
    public function setBillingAddress(\Magento\Quote\Api\Data\AddressInterface $billingAddress)
    {
        $this->_quote->setBillingAddress($billingAddress);
        return $this;
    }

    /**
     * @param int|null $customerId
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return $this
     */
    public function setCustomer($customerId, \Magento\Customer\Api\Data\CustomerInterface $customer)
    {
        $this->_quote->setCustomerId($customerId)
            ->setCustomerIsGuest(true)
            ->setCustomer($customer);
        return $this;
    }

    /**
     * @return $this
     */
    public function collectTotals()
    {
        $this->_quote->collectTotals();
        return $this;
    }

    public function getQuote()
    {
        return $this->_quote;
    }

    public function saveQuote()
    {
        $this->_quote->save();
        return $this->_quote;
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getOrder()
    {
        return $this->_orderRepository->get($this->order->getId());
    }

    public function placeOrder()
    {
        $this->_quote->save();
        return $this->_orderRepository->get(
            $this->_quoteManagement->placeOrder($this->_quote->getId())
        );
    }


    /**
     * @param \Technify\Dsn\Api\OrderStatusInterface $orderStatus
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateOrderStatus(\Technify\Dsn\Api\OrderStatusInterface $orderStatus)
    {
        switch ($orderStatus->getState())
        {
            case SalesOrderModel::STATE_COMPLETE:
            case SalesOrderModel::STATE_CANCELED:
            case SalesOrderModel::STATE_CLOSED:
            case SalesOrderModel::STATE_HOLDED:
            case SalesOrderModel::STATE_PROCESSING:
                $this->changeOrderState($orderStatus);
            break;
            case SalesOrderModel::ACTION_FLAG_SHIP:
                $this->updateOrderStateToShip($orderStatus);
                break;
            default:break;
        }

        return $this;
    }

    /**
     * @param \Technify\Dsn\Api\OrderStatusInterface $orderStatus
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateOrderStateToShip($orderStatus){

        if (! $this->order->canShip()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('You cant create the Shipment.') );
        }

        $convertOrder = $this->_objectManager->create('Magento\Sales\Model\Convert\Order');
        $shipment = $convertOrder->toShipment($this->order);

        foreach ($this->order->getAllItems() AS $orderItem)
        {
            if (! $orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }
            $orderItem->setIsVirtual(0);
            $qtyShipped = $orderItem->getQtyToShip();

            $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);

            $shipmentItem
                ->setOrderItemId($orderItem->getId())
                ->setRowTotal($orderItem->getRowTotal());

            $shipment->addItem($shipmentItem);
        }

        $shipment->setTracks($orderStatus->getTracks());
        $shipment->addComment($orderStatus->getComment());
        $shipment->register();
        $shipment->getOrder()->setIsInProcess(true);
        try {
            $shipment->save();
            $shipment->getOrder()->save();
            $this->_objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
                ->notify($shipment);
            $shipment->save();
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }
    }

    private function changeOrderState(\Technify\Dsn\Api\OrderStatusInterface $orderStatus)
    {
        $orderService = $this->_objectManager->get('\Magento\Sales\Model\Service\OrderService');

        $orderService
            ->setState(
                $this->order,
                $orderStatus->getState(),
                $orderStatus->getStatus(),
                $orderStatus->getComment(),
                $orderStatus->getNotify(),
                false
            );
    }
}