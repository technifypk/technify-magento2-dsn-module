<?php

namespace Technify\Dsn\Helper {

    class RequestGateway
    {

        private $params = array();
        private $endpoint;
        private $headers;
        protected $api_key;

        /** @var \Technify\Dsn\Helper\Help $_helper*/
        private $_helper;

        public function __construct(
            \Technify\Dsn\Helper\Help $helper
        )
        {
            $this->_helper = $helper;
        }

        public function push($action, $data)
        {
            $this->endpoint = 'http://beta.technify.pk/';
            $this->makeRequest($action, $data)->setApiToken()->send();
        }

        public function get($action, $params)
        {
            $this->endpoint = 'http://api.technify.pk/vendor';
            return $this->makeRequest($action, $params)->setApiToken()->send(true);
        }

        public function authorize($data, $token)
        {
            $this->endpoint = 'http://api.technify.pk/vendor';
            return $this->makeRequest('authorize', $data, $token)->send(true);
        }

        public function setApiToken()
        {
            $this->api_key = $this->_helper->getTechnifyAuthToken();
            return $this;
        }

        private function makeRequest($action, $data, $token = false)
        {
            $this->params['payload'] = json_encode(array(
                'action' => $action,
                'dataPacket' => $data,
                'time_stamp' => date("Y-m-d H:i:s")
            ));

            $this->headers = array(
                'Authorization' => ($token) ? $token : $this->api_key,
                'Accept' => 'application/json'
            );

            return $this;
        }

        private function send($result = false)
        {
            $request_headers = $this->get_header_array($this->headers);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->endpoint,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $this->params,
                CURLOPT_HTTPHEADER => $request_headers,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            return (!$result) ?: $response;
        }

        private function get_header_array($headers = [])
        {
            $header = [];
            foreach ($headers as $header_key => $header_value) {
                $header[] = $header_key . ":" . $header_value;
            }
            return $header;
        }
    }
}