<?php
namespace Technify\Dsn\Api;

/**
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
interface OrderInterface
{

    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */

    const ORDER_ID = 'order_id';

    const CUSTOMER = 'customer';

    const SHIPPING_ADDRESS = 'shipping_address';

    const BILLING_ADDRESS = 'billing_address';

    const SHIPPING_METHOD = 'shipping_method';

    const PAYMENT_METHOD = 'payment_method';

    const ITEMS = 'items';

    const ORDER_STATUS = 'order_status';


    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param \Magento\Quote\Api\Data\AddressInterface $shippingAddress
     * @param \Magento\Quote\Api\Data\AddressInterface $billingAddress
     * @param string $shippingMethod
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\CartItemInterface[] $items
     * @return \Magento\Sales\Api\Data\OrderInterface
     */


    public function create(
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        \Magento\Quote\Api\Data\AddressInterface $shippingAddress,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress,
        $shippingMethod,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        array $items
    );


    /**
     * @param string $orderId
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param \Magento\Quote\Api\Data\AddressInterface $shippingAddress
     * @param \Magento\Quote\Api\Data\AddressInterface $billingAddress
     * @param string $shippingMethod
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\CartItemInterface[] $items
     * @param \Technify\Dsn\Api\OrderStatusInterface $orderStatus
     * @return \Magento\Sales\Api\Data\OrderInterface
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */

    public function update(
        $orderId,
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        \Magento\Quote\Api\Data\AddressInterface $shippingAddress,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress,
        $shippingMethod,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        array $items,
        \Technify\Dsn\Api\OrderStatusInterface $orderStatus
    );


    /**
     * @param string $orderId
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function get($orderId);
}