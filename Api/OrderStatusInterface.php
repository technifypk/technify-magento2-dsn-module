<?php

namespace Technify\Dsn\Api;

interface OrderStatusInterface
{
    const STATUS  = "status";
    const STATE  = "state";
    const COMMENT       = "comment";
    const NOTIFY        = "notify";
    const TRACKS        = "tracks";


    /**
     * @param \Magento\Sales\Api\Data\ShipmentTrackInterface[] $tracks
     * @return mixed
     */
    public function setTracks(array $tracks);


    /**
     * @return \Magento\Sales\Api\Data\ShipmentTrackInterface[] $tracks
     */
    public function getTracks();


    /**
     * @param string $status
     * @return void
     */
    public function setState($state);


    /**
     * @return string
     */
    public function getState();

    /**
     * @param boolean $status
     * @return void
     */
    public function setStatus($status);


    /**
     * @return boolean
     */
    public function getStatus();


    /**
     * @param string $comment
     * @return $this
     */
    public function setComment($comment);


    /**
     * @return string
     */
    public function getComment();


    /**
     * @param boolean $notify
     * @return $this
     */
    public function setNotify($notify);

    /**
     * @return boolean
     */
    public function getNotify();

}